#include <asf.h>

#include <sys/time.h>
#include <inttypes.h>
#include <string.h>

#include <common.h>
#include <common_system.h>
#include <common_pinmux.h>
#include <common_debug.h>
#include <common_clock.h>
#include <common_console.h>
#include <common_time.h>
#include <common_i2c.h>
#include <device_sht30.h>
#include <common_delay.h>
#include <device_wifi.h>
#include <device_hc595.h>
#include <common_pwm.h>
#include <device_button.h>

// [{"count":"11","temperature":"11","humidity":"11"},{"count":"22","temperature":"22","humidity":"22"}]

static int g_running;

static int count;
static int dont_push;

enum{
	APPLICATION_STATE_START,
	APPLICATION_STATE_RUNNING,
	APPLICATION_STATE_DATA_REQUEST,
};

static unsigned int g_application_state;

struct application_module {
	const char *name;
	int(*init)(void);
	int(*process)(void);
	void(*uninit)(void);
};

struct application_module g_modules_00[]={
		{"system" ,synapse_system_init ,synapse_system_process ,synapse_system_uninit },
		{"clock"  ,synapse_clock_init  ,synapse_clock_process  ,synapse_clock_uninit  },
		{"console",synapse_console_init,synapse_console_process,synapse_console_uninit},
		{"time"   ,synapse_time_init   ,synapse_time_process   ,synapse_time_uninit   },
		{"i2c"    ,synapse_i2c_init    ,synapse_i2c_process    ,synapse_i2c_uninit    },
		{"sht30"  ,synapse_sht30_init  ,synapse_sht30_process  ,synapse_sht30_uninit  },
		{"delay"  ,synapse_delay_init  ,synapse_delay_process  ,synapse_delay_uninit  },
		{"wifi"   ,synapse_wifi_init   ,synapse_wifi_process   ,synapse_wifi_uninit   },
		{ "hc595" ,synapse_hc595_init  ,synapse_hc595_process  ,synapse_hc595_uninit  },
        	//{ "pwm"   ,synapse_pwm_init    ,synapse_pwm_process    ,synapse_pwm_uninit    },
        	{ "button",synapse_button_init ,synapse_button_process ,synapse_button_uninit },
};

#define SENSOR_DATA_STR_BUFFER_SIZE         (1024)
static char g_sensor_data_str_buffer[SENSOR_DATA_STR_BUFFER_SIZE];

static void buffer_copier(char *gsm_side_str_buffer){
	memmove(gsm_side_str_buffer, g_sensor_data_str_buffer, strlen(g_sensor_data_str_buffer));
}

static void json_start(){
	snprintf(g_sensor_data_str_buffer, SENSOR_DATA_STR_BUFFER_SIZE, "%s","POST / HTTP/1.1\r\nHost: 34.67.16.50\r\nContent-Type: application/json\r\nContent-Length:");
	snprintf(g_sensor_data_str_buffer+strlen(g_sensor_data_str_buffer), SENSOR_DATA_STR_BUFFER_SIZE, "%s%s","62","\r\n\r\n");
}

static void json_add(int count, int32_t temperature, int32_t humidity){
	snprintf(g_sensor_data_str_buffer+strlen(g_sensor_data_str_buffer),SENSOR_DATA_STR_BUFFER_SIZE, "%s%d%s","{\"count\":\"",count,"\",");
	snprintf(g_sensor_data_str_buffer+strlen(g_sensor_data_str_buffer),SENSOR_DATA_STR_BUFFER_SIZE, "%s%"PRId32"%s","\"temperature\":\"",temperature,"\",");
	snprintf(g_sensor_data_str_buffer+strlen(g_sensor_data_str_buffer),SENSOR_DATA_STR_BUFFER_SIZE, "%s%"PRId32"%s","\"humidity\":\"",humidity,"\"}");
}

static void json_comma(){
	snprintf(g_sensor_data_str_buffer+strlen(g_sensor_data_str_buffer), SENSOR_DATA_STR_BUFFER_SIZE, "%s",",");
}

static void json_finish(){
	snprintf(g_sensor_data_str_buffer+strlen(g_sensor_data_str_buffer), SENSOR_DATA_STR_BUFFER_SIZE, "%s","\r\n\r\n");
	dont_push = 1;
}

static int sensor_data_push (int32_t temperature, int32_t humidity)
{
        //struct tm push_tm;
        //uint32_t push_time;
        //rc = synapse_s35390a_get_datetime(&push_tm);
        //push_time = mktime(&push_tm);

	if(strlen(g_sensor_data_str_buffer) >= 924){
		synapse_infof("sensor buffer full!");
		return 0;
	}

	count++;
	if(strlen(g_sensor_data_str_buffer) == 0){
		json_start();
	}else{
		json_comma();
	}

	json_add(count,temperature,humidity);	
        synapse_infof("sensor data str push length: %d", strlen(g_sensor_data_str_buffer));

        return 0;
}

static int sensors_data_push (void)
{
	if(dont_push){
		return 0;
	}

        {
		int rc = 0;	                
		int32_t temperature = 0;
                int32_t humidity = 0;

		rc = synapse_sht30_get_temperature_and_humidity_single_shot (&temperature,&humidity);
		synapse_infof("Temperature : %ld Humidity : %ld",temperature,humidity);                
		synapse_infof("return count : %d",rc);
		sensor_data_push(temperature, humidity);
        }

        return 0;
}

int main (void)
{
	//uint16_t status_reg = 8888;

	int rc = 0;
	uint32_t m = 0;
	uint32_t iteration = 0;

        uint32_t sensorTick = 0;
        uint32_t dataTick = 0;

        //uint32_t indicatorTick = 0;
        //uint32_t indicatorOnTick = 0;
        uint32_t hmiState = 0;

        //uint32_t applicationIndicatorTick = 250;
        uint32_t applicationLoopTick = 120;
        uint32_t applicationSensorTick = 25000; //19sn	
        uint32_t applicationDataTick = 30000; //30sn

	for (m = 0; m < sizeof(g_modules_00) / sizeof(g_modules_00[0]); m++) {
		g_modules_00[m].init();
	}

        //synapse_pwm_run(50);

	synapse_pinmux_setup(PIN_BUZZER,1,1,0);
        synapse_delay_msleep(75);
        synapse_pinmux_setup(PIN_BUZZER,1,0,0);

        //synapse_pwm_start();
        //synapse_delay_msleep(50);
        //synapse_pwm_stop();

	g_running = 1;

	g_application_state = APPLICATION_STATE_START;

        synapse_hc595_set_text("--");
        //synapse_hc595_set_gauge(4);
        //synapse_hc595_set_indicator1(0);
        //synapse_hc595_set_indicator2(0);
        //synapse_hc595_set_indicator3(0);
        synapse_hc595_set_on();

	for(iteration=0; g_running!=0; iteration++)
	{
		uint32_t loopStart;
        	uint32_t loopFinish;
		unsigned int wfiState;
        	unsigned int wfiStateStatus;
        	struct timeval currentTimeval;


		gettimeofday(&currentTimeval, NULL);
        	loopStart = currentTimeval.tv_sec * 1000 + currentTimeval.tv_usec / 1000;

		for (m = 0; m < sizeof(g_modules_00) / sizeof(g_modules_00[0]); m++) {
			g_modules_00[m].process();
		}

		//sht30_read_status(&status_reg);
		//synapse_infof("Status Register : %u",(unsigned int)status_reg);

		wfiState = synapse_wifi_get_state();
	       	wfiStateStatus = synapse_wifi_get_state_status();

//== HMI CODE START

//                if ((loopStart < indicatorTick) || ((loopStart - indicatorTick) > applicationIndicatorTick)) {
		  if(1){
                                {
                                        int value;
                                        //indicatorOnTick = loopStart;
                                        synapse_hc595_set_on();
                                        switch (hmiState%2){
                                                case 0: {
					                int32_t temperature = 0;
					                int32_t humidity = 0;
							synapse_sht30_get_temperature_and_humidity_single_shot (&temperature,&humidity);
							value = temperature;
							synapse_hc595_set_float((float) value / 10000000.00);
							break;
                                                }
                                                case 1: {
					                int32_t temperature = 0;
					                int32_t humidity = 0;
							synapse_sht30_get_temperature_and_humidity_single_shot (&temperature,&humidity);
							value = humidity;
							synapse_hc595_set_float((float) value / 10000000.00);
							break;
                                                }
                                                default:
                                                        //value = 2;
                                                        break;
                                        }
	                                if(synapse_button_get_pressed()){
                                      		hmiState++;
						synapse_pinmux_setup(PIN_BUZZER,1,1,0);
        					synapse_delay_msleep(50);
        					synapse_pinmux_setup(PIN_BUZZER,1,0,0);
					}
                                }

                        //indicatorTick = loopStart;
                }

//== HMI CODE FINISH

		if (g_application_state == APPLICATION_STATE_START){
			g_application_state = APPLICATION_STATE_RUNNING;
		} else if (g_application_state == APPLICATION_STATE_RUNNING){
                        if ((loopStart < sensorTick) ||
                           ((loopStart - sensorTick) > applicationSensorTick)) {
                                synapse_debugf("sensor tick");
                                rc = sensors_data_push();
                                if (rc != 0) {
                                        synapse_errorf("sensors_data_push failed");
                                        goto reset;
                                }
                                sensorTick = loopStart;
                        }	
                	if ((loopStart < dataTick) || ((loopStart - dataTick) > applicationDataTick)) {		
				if (wfiState == SYNAPSE_WIFI_STATE_ATOK) {
					if (wfiStateStatus == SYNAPSE_WIFI_STATE_STATUS_SUCCESS) {
						synapse_wifi_initial();
					} else if (wfiStateStatus == SYNAPSE_WIFI_STATE_STATUS_ERROR) {
						synapse_errorf("wifi state atok error");
						goto reset;
					}
				} else if (wfiState == SYNAPSE_WIFI_STATE_INITIAL) {
					if (wfiStateStatus == SYNAPSE_WIFI_STATE_STATUS_SUCCESS) {
						g_application_state = APPLICATION_STATE_DATA_REQUEST;
						json_finish();
						synapse_infof("%s",g_sensor_data_str_buffer);
						synapse_wifi_connect();
						buffer_copier_caller(buffer_copier);
					} else if (wfiStateStatus == SYNAPSE_WIFI_STATE_STATUS_ERROR) {
						synapse_errorf("wifi state initial error");
						goto reset;
					}
				} else {
	               			synapse_errorf("unknown wfi state : %d", wfiState);
	               			goto reset;
				}		
			}
		} else if (g_application_state == APPLICATION_STATE_DATA_REQUEST){
                        if ((loopStart < sensorTick) ||
                           ((loopStart - sensorTick) > applicationSensorTick)) {
                                synapse_debugf("sensor tick");
                                rc = sensors_data_push();
                                if (rc != 0) {
                                        synapse_errorf("sensors_data_push failed");
                                        goto reset;
                                }
                                sensorTick = loopStart;
                        }
			if (wfiState == SYNAPSE_WIFI_STATE_CONNECT) {
				if (wfiStateStatus == SYNAPSE_WIFI_STATE_STATUS_SUCCESS) {
					//synapse_wifi_close(???);
					synapse_wifi_atok();
					g_sensor_data_str_buffer[0]='\0';
					dont_push = 0;
					g_application_state = APPLICATION_STATE_RUNNING;
				} else if (wfiStateStatus == SYNAPSE_WIFI_STATE_STATUS_ERROR) {
					synapse_errorf("wifi state connect error");
					goto reset;
				}
			} else {
                        	synapse_errorf("unknown wfi state: %d", wfiState);
                        	goto reset;
                	}
        	} else {
                	synapse_errorf("unknown application state: %d", g_application_state);
                	goto reset;
        	}

        	gettimeofday(&currentTimeval, NULL);
        	loopFinish = currentTimeval.tv_sec * 1000 + currentTimeval.tv_usec / 1000;

		if ((loopFinish >= loopStart) && ((loopFinish - loopStart) < applicationLoopTick)) {
	        	synapse_delay_msleep(applicationLoopTick - (loopFinish - loopStart));
        	}
	}

	for (m = 0; m < sizeof(g_modules_00) / sizeof(g_modules_00[0]); m++) {
        	g_modules_00[m].uninit();
	}

reset:
	synapse_infof("bad reset :(");

}
